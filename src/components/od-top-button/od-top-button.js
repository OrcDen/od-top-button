const template = document.createElement( "template" );
template.innerHTML = `
    <style>
        :host([active]) {
            display: block;
        }

        :host(:not([active])) {
            display: none;
        }
        
        #top-button {
            display: flex;
            justify-content: center;
            position: fixed;
            cursor: pointer;
            bottom: 0;
                        
            transition: all .2s ease-in-out 0s;

            font-size: 2rem;

            background-color: #333;
            color: #fff;

            right: 2em;

            height: .75em;
            width: 2em;

            z-index: 100;

            outline: none;

            border: none;
            border-radius: .1em .1em 0 0;
        }

        #top-button:hover {
            background-color: #555;
        }        

    </style>
    
    <button id='top-button' title='Go to top' part='button'>^</button>
`;

window.ShadyCSS && window.ShadyCSS.prepareTemplate( template, "od-top-button" );

export class OdTopButton extends HTMLElement {
    constructor() {
        super();
        //constructor creates the shadowRoot
        window.ShadyCSS && window.ShadyCSS.styleElement( this );
        if ( !this.shadowRoot ) {
            this.attachShadow( { mode: "open" } );
            this.shadowRoot.appendChild( template.content.cloneNode( true ) );
        }
    }

    connectedCallback() {
        //set attribute default values first
        if ( !this.hasAttribute( "offset" ) ) {
            this.setAttribute( "offset", 1 );
        }

        //all properties should be upgraded to allow lazy functionality
        this._upgradeProperty( "offset" );
        this._upgradeProperty( "active" );

        //listeners and others etc.
        this.buttonElement = this.shadowRoot.querySelector( "#top-button" );
        this.buttonElement.addEventListener( "click", () => {
            this.toTop();
        } );

        window.addEventListener( "scroll", () => {
            this._setActive();
        } );
    }

    disconnectedCallback() {
        //window listeners should always be disconnected since they are above our shadowRoot
        window.removeEventListener( "scroll", () => {
            this._setActive();
        } );
    }

    //General - every element should have this
    _upgradeProperty( prop ) {
        if ( Object.prototype.hasOwnProperty.call( this, prop ) ) {
            var value = this[prop];
            delete this[prop];
            this[prop] = value;
        }
    }

    static get observedAttributes() {
        //only observe the attributes you expect to change internally/externally
        return ["active", "offset"];
    }

    attributeChangedCallback( attrName, oldValue, newValue ) {
        //handle side effects/extra logic - DO NOT SET the associated property here
        //handle invald attribute values
        switch ( attrName ) {
            case "offset": {
                if ( !this._validateOffset( newValue ) ) {
                    this.setAttribute( "offset", oldValue );
                }
                break;
            }
            case "active": {
                if ( newValue !== null && newValue !== "" && !this._validateBoolean( newValue ) ) { //boolean type safety: null - removed, "" - set true
                    this._setActiveAttribute( oldValue );
                    break;
                }
                if( newValue !== oldValue ) {
                    this._setActiveAttribute( newValue );
                }
                break;
            }
        }
    }

    //properties - all 'custom attributes' should have a getter and setter to reflect the attribute
    get active() {
        return this.hasAttribute( "active" );
    }

    set active( isActive ) {
        if ( typeof isActive !== "boolean" ) {
            return;
        }
        this._setActiveAttribute( isActive );
    }

    _setActiveAttribute( newV ) {
        this._setBooleanAttribute( 'active', newV );
    }

    _validateBoolean( newV ) {
        return (
            typeof newV === "boolean" || newV === "true" || newV === "false"
        );
    }

    _setBooleanAttribute( name, newV ) {
        if ( newV !== "" && ( !newV || newV === "false" ) ) {
            this.removeAttribute( name );
        } else {
            this.setAttribute( name, true );
        }
    }

    get offset() {
        return parseInt( this.getAttribute( "offset" ) );
    }

    set offset( offsetValue ) {
        if ( typeof offsetValue !== "number" ) {
            return;
        }
        if ( this._validateOffset( offsetValue ) ) {
            this.setAttribute( "offset", offsetValue );
        }
    }

    _validateOffset( newV ) {
        let temp = parseInt( newV );
        return !isNaN( temp ) && temp >= 0;
    }

    toTop() {
        document.documentElement.scrollTop = 0; // For Chrome, Firefox, IE and Opera
        document.body.scrollTop = 0; // For Safari
    }

    //internal functions should begin with '_'
    _setActive() {
        this.active = this._calulateActive();
    }

    _calulateActive() {
        return (
            document.documentElement.scrollTop > this.offset || document.body.scrollTop > this.offset
        );
    }
}
