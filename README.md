# <od-top-button>

> Button to scroll the user's screen to the top.

`<od-top-button>` is a simple button that will show and hide, based on a givin screen offset. When clicked the screen will scroll to the top. By default it has an offset of 1px.

## Installation
- Install with [npm](https://www.npmjs.com/)

```
npm i @orcden/od-top-button
```
## Usage
```
import '@orcden/od-top-button';
```
```
<od-top-button offset=5></od-top-button>
```
    
## Attributes
| Attribute | Type | Default | Description                                                                             |
|-----------|---------|---------|-----------------------------------------------------------------------------------------|
| `offset`  | Number  | 1       | In pixels. Set to change how far down the page the user has to scroll before the button is active.  |
| `active`  | Boolean | false   | Controls CSS to show/hide the button. Setting offset to 0 sets the button to always active. |

## Functions
| Name | Parameters | Description                                  |
|-----------|------|-----------------------------------------------|
| `toTop`   | None | Scrolls the users screen to the top. |

## Styling
- CSS variables are available to alter the default styling provided

| Shadow Parts     | Description           |
|------------------|-----------------------|
| button           | The html button used inside the component |

## Development
### Run development server and show demo

```
npm run demo
```

### Run linter

```
npm run lint
```

### Fix linter errors

```
npm run fix
```

### Run tests

```
npm run test
```

### Build for production

```
npm run build
```